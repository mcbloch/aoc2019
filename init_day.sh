#!/bin/bash

if (( $# != 1 )); then
    echo "Illegal number of parameters"
    echo "Please provide a day number like this: \"$0 01\"" && exit
fi

source .env
DAY=$1
./get-input.sh $DAY || exit

FILE=`cat <<EOF
(ns aoc2019.day${DAY})
(defn ex1 [])
(defn ex2 [])

(defn -main []
  {"Day" "${DAY}"
   "Description" ""
   "Part1" (ex1)
   "Part2" (ex2)})
EOF
`
TEST=`cat <<EOF
(ns aoc2019.day${DAY}-test
 (:require [clojure.test :refer :all]
  [aoc2019.day${DAY} :refer :all]))

(deftest test-ex1
 (testing "examples"
  (is (= 1 0))))

(deftest test-ex2
 (testing "examples"
  (is (= 1 0))))
EOF
`


echo $FILE > src/aoc2019/day$DAY.clj
echo $TEST > test/aoc2019/day${DAY}_test.clj
