(defproject aoc2019 "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "EPL-2.0 OR GPL-2.0-or-later WITH Classpath-exception-2.0"
            :url "https://www.eclipse.org/legal/epl-2.0/"}
  :dependencies [[org.clojure/clojure "1.10.0"]
                 [org.clojure/math.combinatorics "0.1.6"]
                 [org.clojure/math.numeric-tower "0.0.4"]
                 [org.clojure/core.async "0.4.500"]
                 [com.jakemccrary/lein-test-refresh "0.24.1"]
                 [org.clojure/core.cache "0.8.2"]
                 [quil "3.1.0"]
                 [criterium "0.4.5"]]
  :main ^:skip-aot aoc2019.core
  :test-paths ["test"]
  :target-path "target/%s"
  :repl-options {:init-ns aoc2019.user}
  :profiles {:uberjar {:aot :all}
             :dev {:plugins [[com.jakemccrary/lein-test-refresh "0.24.1"]
                             [venantius/ultra "0.6.0"]]}})
