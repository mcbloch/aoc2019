(ns aoc2019.day11-test
  (:require [clojure.test :refer :all] [aoc2019.day11 :refer :all]))

(deftest test-ex1 (testing "answer" (is (= 1894 (ex1)))))

(deftest test-ex2 (testing "examples" (is (= 1 0))))
