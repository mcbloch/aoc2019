(ns aoc2019.day01-test
  (:require [clojure.test :refer :all]
            [aoc2019.day01 :refer :all]))

(deftest test-ex1
  (testing "simple"
    (is (= 3315133 (ex1)))))

(deftest test-calculate-fuel
  (is (= 2 (calculate-fuel 12)))
  (is (= 2 (calculate-fuel 14)))
  (is (= 654 (calculate-fuel 1969)))
  (is (= 33583 (calculate-fuel 100756))))

(deftest test-ex2
  (testing "examples"
    (is (= 2 (calculate-fuel-inclusive 14)))
    (is (= 966 (calculate-fuel-inclusive 1969)))
    (is (= 50346 (calculate-fuel-inclusive 100756))))
  (testing "wrongs"
    (is (> 4972655 (ex2)))))
