(ns aoc2019.day05-test (:require [clojure.test :refer :all] [aoc2019.day05 :refer :all]))

(deftest test-ex1
  (testing "solution"
    (is (= 16225258 (ex1)))))

(deftest Equals-8-position-mode
  (let [data '(3,9,8,9,10,9,4,9,99,-1,8)]
    (is (= 0 (ex2 data 4)))
    (is (= 1 (ex2 data 8)))))

(deftest Lower-then-8-position-mode
  (let [data '(3,9,7,9,10,9,4,9,99,-1,8)]
    (is (= 1 (ex2 data 4)))
    (is (= 1 (ex2 data 7)))
    (is (= 0 (ex2 data 8)))
    (is (= 0 (ex2 data 9)))
    (is (= 0 (ex2 data 20)))))

(deftest Equals-8-immediate-mode
  (let [data '(3,3,1108,-1,8,3,4,3,99)]
    (is (= 0 (ex2 data 4)))
    (is (= 1 (ex2 data 8)))))

(deftest  Lower-then-8-immediate-mode
  (let [data '(3,3,1107,-1,8,3,4,3,99)]
    (is (= 1 (ex2 data 4)))
    (is (= 1 (ex2 data 7)))
    (is (= 0 (ex2 data 8)))
    (is (= 0 (ex2 data 9)))
    (is (= 0 (ex2 data 20)))))

(deftest Jumps-position-mode
  (let [data '(3,12,6,12,15,1,13,14,13,4,13,99,-1,0,1,9)]
    (is (= 0 (ex2 data 0)))
    (is (= 1 (ex2 data -4)))
    (is (= 1 (ex2 data 1)))
    (is (= 1 (ex2 data 20)))))

(deftest Jumps-immediate-mode
  (let [data '(3,3,1105,-1,9,1101,0,0,12,4,12,99,1)]
    (is (= 0 (ex2 data 0)))
    (is (= 1 (ex2 data -1)))
    (is (= 1 (ex2 data 1)))
    (is (= 1 (ex2 data 4)))))
