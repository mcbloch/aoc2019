(ns aoc2019.day09-test
  (:require
   [clojure.test :refer :all]
   [aoc2019.day09 :refer :all]))

(deftest test-ex1
  (testing "wrong-answer"
    (is (< 203 (first (ex1)))))
  (testing "answer"
    (is (= '(2399197539) (ex1))))

  (testing "examples"
    (is (= '(1125899906842624) (ex1 '(104,1125899906842624,99)))) ; Simply return big number
    (is (= '(1219070632396864) (ex1 '(1102,34915192,34915192,7,4,7,99,0)))) ; Big multiplication
    (let [code '(109,1,204,-1,1001,100,1,100,1008,100,16,101,1006,101,0,99)]
      (is (= code (reverse (ex1 code)))))) ; return copy of self

  (deftest test-ex2
    (testing "answer"
      (is (= '(35106) (ex2))))))
