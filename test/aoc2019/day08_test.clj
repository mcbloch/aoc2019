(ns aoc2019.day08-test
  (:require [clojure.test :refer :all] [aoc2019.day08 :refer :all]))

(deftest test-ex1 (testing "solution"
                    (is (= 2250 (ex1)))))

(deftest test-ex2
  (testing "examples"
    #_(is (= "01\n10" (ex2 2 2 (str->digits "0222112222120000"))))))
