(ns aoc2019.day03-test (:require [clojure.test :refer :all] [aoc2019.day03 :refer :all]))

(def ex-testinput '({:index 0 :moves (["D" 1]
                                      ["L" 2]
                                      ["U" 4])}

                    {:index 1 :moves (["U" 4]
                                      ["L" 4])}

                    {:index 2 :moves (["D" 3])}))

(deftest test-ex1
  (testing "read-moves"
    (is (= ex-testinput
           (read-runs ["D1,L2,U4" "U4,L4" "D3"])))
    (is (= {{:x 0, :y -1} {0 1},
            {:x -1, :y -1} {0 2},
            {:x -2, :y -1} {0 3},
            {:x -2, :y 0} {0 4},
            {:x -2, :y 1} {0 5},
            {:x -2, :y 2} {0 6},
            {:x -2, :y 3} {0 7}}
           (process-run (first ex-testinput) {})))
    (is (= 159 (ex1 (read-runs ["R75,D30,R83,U83,L12,D49,R71,U7,L72"
                                "U62,R66,U55,R34,D71,R55,D58,R83"]))))
    (is (= 135 (ex1 (read-runs ["R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51"
                                "U98,R91,D20,R16,D67,R40,U7,R15,U6,R7"]))))
    (let [res (ex1)]
      (is (> 9215 res))
      (is (< 157 res))
      (is (< 28 res))
      (is (= 209 res)))))

(deftest test-ex2
  (testing "examples"
    (is (= 43258 (ex2)))))
