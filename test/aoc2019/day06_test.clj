(ns aoc2019.day06-test (:require [clojure.test :refer :all] [aoc2019.day06 :refer :all]))

(deftest test-ex1
  (testing "simple"
    (is (= 42 (ex1 [["COM" "B"] ["B" "C"] ["C" "D"] ["D" "E"] ["E" "F"] ["B" "G"] ["G" "H"] ["D" "I"] ["E" "J"] ["J" "K"] ["K" "L"]])))
    (is (= 44 (ex1 [["COM" "B"] ["B" "C"] ["C" "D"] ["D" "E"] ["E" "F"] ["B" "G"] ["G" "H"] ["D" "I"] ["E" "J"] ["J" "K"] ["K" "L"] ["B" "M"]])))
    (is (= 46 (ex1 [["COM" "B"] ["B" "C"] ["C" "D"] ["D" "E"] ["E" "F"] ["B" "G"] ["G" "H"] ["D" "I"] ["E" "J"] ["J" "K"] ["K" "L"] ["B" "M"] ["B" "N"]])))
    (is (= 47 (ex1 [["COM" "B"] ["B" "C"] ["C" "D"] ["D" "E"] ["E" "F"] ["B" "G"] ["G" "H"] ["D" "I"] ["E" "J"] ["J" "K"] ["K" "L"] ["B" "M"] ["B" "N"] ["COM" "O"]]))))

  (testing "wrong-answers"
    (is (= 144909 (ex1)))))

(deftest test-ex2
  (let [input [["COM" "B"] ["B" "C"] ["C" "D"] ["D" "E"] ["E" "F"] ["B" "G"] ["G" "H"] ["D" "I"] ["E" "J"] ["J" "K"] ["K" "L"]]
        input-to-map (into {} (map (comp vec reverse) input))]
    (testing "build-path"
      (is (= '("D" "C" "B" "COM") (build-path input-to-map "I")))
      (is (= '("G" "B" "COM") (build-path input-to-map "H"))))
    (testing "combined path"
      (is (= 3 (transfer-count "I" "H" input-to-map))))))

(deftest faster
  (testing "something"
    (is (= (faster?) (ex1)))))
