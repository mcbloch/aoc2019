(ns aoc2019.day04-test (:require [clojure.test :refer :all]
                                 [aoc2019.day04 :refer :all]))

(deftest test-ex1
  (testing "numTodigits"
    (is (= [1 2 3] (num-to-digits 123))))
  (testing "not-decreasing"
    (is (true? (is-not-decreasing [1 2 3])))
    (is (true? (is-not-decreasing [2 2 2])))
    (is (false? (is-not-decreasing [3 2 3])))
    (is (false? (is-not-decreasing [1 2 1])))
    (is (false? (is-not-decreasing [1 4 3])))
    (is (false? (is-not-decreasing [1 4 3]))))
  (testing "at-least-2-adjecent"
    (is (false? (at-least-2-adjecent [1 2 3 4])))

    (is (true? (at-least-2-adjecent [1 2 2 4])))
    (is (true? (at-least-2-adjecent [1 2 3 3])))
    (is (true? (at-least-2-adjecent [2 2 3 4]))))
  (testing "password"
    (is (true? (check-password 111111)))
    (is (false? (check-password 223450)))
    (is (false? (check-password 123789))))
  (testing "result"
    (is (= 1855 (ex1)))))

(deftest test-ex2
  (testing "exact-2-adjecent"
    (is (false? (exact-2-adjecent [1 2 3 4])))
    (is (true? (exact-2-adjecent [1 2 4 4 5])))

    (is (true? (exact-2-adjecent [1 2 2 4])))
    (is (true? (exact-2-adjecent [1 2 3 3])))
    (is (true? (exact-2-adjecent [2 2 3 4])))

    (is (false? (exact-2-adjecent [2 2 2 4])))
    (is (false? (exact-2-adjecent [1 2 2 2])))
    (is (true? (exact-2-adjecent [2 2 2 4 4]))))

  (testing "result"
    (is (= 1253 (ex2)))))
