(ns aoc2019.day02-test (:require [clojure.test :refer :all] [aoc2019.day02 :refer :all]))

(deftest test-ex1
  (testing "example"
    (is (= [2 0 0 0 99] (process-intcode [1,0,0,0,99])))
    (is (= [2 3 0 6 99] (process-intcode [2,3,0,3,99])))
    )
  (testing "simple"
    (is (< 165 (ex1)))))
