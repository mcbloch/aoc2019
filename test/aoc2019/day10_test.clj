(ns aoc2019.day10-test
  (:require [clojure.test :refer :all] [aoc2019.day10 :refer :all]))

(deftest test-ex1
  (testing "ex"
    (is (= [8 3] (:coordinates (ex1 (read-input "input_10_test_small.txt"))))))
  (testing "solution"
    (is (= {:coordinates [20 18], :visible-meteors 280}  (ex1)))))

(defn coord [meteors idx] (-> meteors
                              (nth (dec idx))
                              :coordinate))

(deftest test-ex2
  (testing "examples"
    (let [vs (vaporize-meteorites "input_10_test.txt")]
      (is (= [11 12] (coord vs 1)))
      (is (= [12 1] (coord vs 2)))
      (is (= [12 2] (coord vs 3)))
      (is (= [12 8] (coord vs 10)))
      (is (= [16 0] (coord vs 20)))
      (is (= [16 9] (coord vs 50)))
      (is (= [10 16] (coord vs 100)))
      (is (= [9 6] (coord vs 199)))
      (is (= [8 2] (coord vs 200)))))
  (testing "example_small"
    (let [vs (vaporize-meteorites "input_10_test_small.txt")]
      (is (= [8 1] (coord vs 1)))
      (is (= [9 0] (coord vs 2)))
      (is (= [9 1] (coord vs 3)))
      (is (= [10 0] (coord vs 4)))
      (is (= [9 2] (coord vs 5)))
      (is (= [11 1] (coord vs 6)))
      (is (= [12 1] (coord vs 7)))
      (is (= [11 2] (coord vs 8)))
      (is (= [15 1] (coord vs 9)))

      (is (= [12 2] (coord vs 10)))
      (is (= [13 2] (coord vs 11)))
      (is (= [14 2] (coord vs 12)))
      (is (= [15 2] (coord vs 13)))
      (is (= [12 3] (coord vs 14)))

      (is (= [16 4] (coord vs 15)))
      (is (= [15 4] (coord vs 16)))
      (is (= [10 4] (coord vs 17)))))
  (testing "wrong"
    (is (> 1107 (ex2)))))
