#!/bin/sh

DAY=$(expr $1 + 0)
COOKIE=$AoCCookie

curl \
    -fSL -o resources/input_$DAY.txt \
    -H "cookie:$COOKIE" \
    https://adventofcode.com/2019/day/$DAY/input
