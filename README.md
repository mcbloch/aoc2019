# AoC2019

My submissions for the [Advent of Code](https://adventofcode.com) 2019.

## Creating a new daily entry.

- Login to the website and download your session cookie.
- Save this cookie in your `.env` file in the `AoCCookie` variable.
- Run the init script. It downloads the new input file and makes to files to start coding in.
```
    ./init_day.sh "01"
```

## Running

Running all solutions. This will print them in a table.
```bash
lein run
```

## Tests

```sh
lein test
```

## License
This program and the accompanying materials are made available under the
terms of the Eclipse Public License 2.0 which is available at
http://www.eclipse.org/legal/epl-2.0.

This Source Code may also be made available under the following Secondary
Licenses when the conditions for such availability set forth in the Eclipse
Public License, v. 2.0 are satisfied: GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or (at your
option) any later version, with the GNU Classpath Exception which is available
at https://www.gnu.org/software/classpath/license.html.
