(ns aoc2019.day07
  (:require
   [clojure.java.io :as io]
   [aoc2019.day05 :refer [process-intcode]]
   [clojure.math.combinatorics :as combo]
   [clojure.core.async
    :as a
    :refer [>! <! >!! <!! go chan buffer close! thread
            alts! alts!! take! put! timeout]]))

(defn read-input []
  "Read file, split into array, cast every line to integers"
  (map read-string (-> (slurp
                        (-> "input_7.txt" io/resource io/file))
                       (clojure.string/split #","))))

(defn ex1 []
  (let [input (vec (read-input))]
    (apply max-key :thrust (for [signal (combo/permutations [0 1 2 3 4])]
                             (let [chA (chan 2) chB (chan 2) chC (chan 2) chD (chan 2) chE (chan 2)]
                               (>!! chA (nth signal 0))
                               (>!! chB (nth signal 1))
                               (>!! chC (nth signal 2))
                               (>!! chD (nth signal 3))
                               (>!! chE (nth signal 4))
                               (>!! chA 0)
                               (>!! chB (process-intcode input chA))
                               (>!! chC (process-intcode input chB))
                               (>!! chD (process-intcode input chC))
                               (>!! chE (process-intcode input chD))
                               (let [resE (process-intcode input chE)]
                                 {:thrust resE
                                  :signal signal}))))));; => {:thrust 398674, :signal [0 3 1 2 4]}

(defn ex2
  ([]
   (ex2 (vec (read-input)))) ;; => {:thrust 39431233, :signal [7 8 5 9 6]}
  ([input]
   (println "Testing")
   (apply max-key :thrust (for [signal (combo/permutations [5 6 7 8 9])]
                            (let [chA (chan 3) chB (chan 3) chC (chan 3) chD (chan 3) chE (chan 3)]
                              (>!! chA (nth signal 0))
                              (>!! chB (nth signal 1))
                              (>!! chC (nth signal 2))
                              (>!! chD (nth signal 3))
                              (>!! chE (nth signal 4))

                              (>!! chA 0)

                              (go (process-intcode input chA chB "Amp 1"))
                              (go (process-intcode input chB chC "Amp 2"))
                              (go (process-intcode input chC chD "Amp 3"))
                              (go (process-intcode input chD chE "Amp 4"))
                              (let [res-ch (go (process-intcode input chE chA "Amp 5"))]
                                       ; Propagate values from first round to the next amplifiers
                                {:thrust (<!! res-ch)
                                 :signal signal}))))))

(defn -main [] {"Day" "07" "Description" "" "Part1" (ex1) "Part2" (ex2)})
