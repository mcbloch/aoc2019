(ns aoc2019.day08
  (:require
   [clojure.java.io :as io]
   [clojure.zip :as zip]
   [clojure.math.combinatorics :as combo]
   [clojure.spec.alpha :as s]
   [clojure.core.async
    :as a
    :refer [>! <! >!! <!! go chan buffer close! thread
            alts! alts!! take! put! timeout]]
   [criterium.core :refer [bench with-progress-reporting]]))

(defn str->digits [s]
  (map (comp read-string str) (seq s)))

(defn read-input []
  "Read file, split into array, cast every line to integers"
  (str->digits
   (-> "input_8.txt" io/resource io/file slurp
       clojure.string/trim-newline)))

(def width 25)
(def height 6)
(defn pixels-in-layer [w h] (* w h))

(defn split-in-chunks [chunksize coll]
  (map #(take chunksize %)
       (take-while seq (iterate #(second (split-at chunksize %)) coll))))

(defn ex1 []
  (let [pil (pixels-in-layer width height)]
    (-> (read-input)
        ((partial split-in-chunks pil))
        vec
        ((fn [layers] (->> layers
                           (map frequencies)
                           (apply min-key #(get % 0))
                           ((fn [img] (* (get img 1) (get img 2)))))))))) ;; => 2250

(defn ex2
  ([] (ex2 width height (read-input)))
  ([w h i]
   {:pre [(s/conform (s/* int?) i)]}
   (let [pil (pixels-in-layer w h)]
     (-> i
         ((partial split-in-chunks pil))
         vec
         ((fn [layers] (->> layers
                            (apply map vector)
                            (map (comp first (partial drop-while #(= 2 %)) reverse))
                            (split-in-chunks width)
                            (map clojure.string/join)
                            (clojure.string/join "\n"))))))))
;; => FHJUL

(defn -main []
  {"Day" "08" "Description" "Decoding an image" "Part1" (ex1) "Part2" (str (subs (ex2) 0 10) "...")})
