;;; day03.clj: Day 3: Crossed Wires

(ns aoc2019.day03
  (:require [clojure.java.io :as io]
            [clojure.math.numeric-tower :as math]))

(def direction-offset {"U" {:x 0 :y 1}
                       "R" {:x 1 :y 0}
                       "L" {:x -1 :y 0}
                       "D" {:x 0 :y -1}})

(defn read-moves [moves]
  (map (fn [move] [(subs move 0 1)
                   (read-string (subs move 1))])
       moves))

(defn read-runs [runs]
  (map-indexed (fn [idx itm]
                 ;; (println "Reading run" idx)
                 {:index idx
                  :moves (-> itm
                             (clojure.string/split #",")
                             read-moves)}) runs))

(defn read-input
  []
  (-> "input_3.txt"
      io/resource
      io/file
      slurp
      clojure.string/split-lines
      read-runs))

(defn advance-move [[dir dist] rest-moves]
  (if (= 1 dist)
    [(first rest-moves)
     (next rest-moves)]
    [[dir (dec dist)]
     rest-moves]))

(defn process-run [{:keys [index moves]} start-map]
  ;; (println "Process run" index "with data" moves)
  (loop [move (first moves)
         rest-moves (next moves)
         current {:x 0 :y 0}
         map start-map
         step-distance 1]
    (if (nil? move)
      map
      (let [[direction _] move
            next-pos (merge-with + current (get direction-offset direction))
            updated-map (update map next-pos #(merge {index step-distance} %))
            [*move *rest-moves] (advance-move move rest-moves)]
        ;; (println "Command is " move)
        ;; (println "Go from" current "to" next-pos)
        ;; (println "Next move" move "->" *move)
        (recur
          *move
          *rest-moves
          next-pos
          updated-map
          (inc step-distance))))))

(defn calculate-closest-intersection
  [circuit wirecount manhattan-dist]
  (->> circuit
       (filter (fn [[k v]] (= (count v) wirecount)))
       (filter (fn [[k v]] (not= {:x 0 :y 0} k)))
       #_((fn [circ] (println "SIZE" (count circ)) circ))
       (map (fn [[{:keys [x y]} v]] (if manhattan-dist
                                      (let [dist (+ (math/abs x) (math/abs y))]
                                        #_(println x y v "\t" dist)
                                        dist)
                                      (let [wirelen-sum (reduce + (vals v))]
                                        wirelen-sum))))
       (reduce min)))

(defn process-input
  [runs manhatten-dist]
  (loop [circuit {}
         run (first runs)
         rest-runs (next runs)]
    ;; (println "Mapstate" map)
    (if (nil? run)
      (calculate-closest-intersection circuit (count runs) manhatten-dist)
      (recur (process-run run (update circuit {:x 0 :y 0} #(assoc % (:index run) 0)))
             (first rest-runs)
             (next rest-runs)))))

(defn ex1
  ([] (ex1 (read-input)))
  ([input]
   (process-input input true)))                             ;; => 209

(defn ex2 []
  (process-input (read-input) false))                       ;; => 43258

(defn -main [] {"Day" "03" "Description" "Distance to the closest wiring collision" "Part1" (ex1) "Part2" (ex2)})
