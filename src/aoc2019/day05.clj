(ns aoc2019.day05
  (:require [clojure.java.io :as io]
            [clojure.core.async
             :as a
             :refer [>! <! >!! <!! go chan buffer close! thread
                     alts! alts!! take! put! timeout]]))

(defn read-opcode-from-file [filename]
  (map read-string (-> (slurp
                        (-> filename io/resource io/file))
                       (clojure.string/split #","))))

(defn read-input []
  "Read file, split into array, cast every line to integers"
  (read-opcode-from-file "input_5.txt"))

(defn num-to-digits
  "Parses a number into a list of its digits"
  [num]
  (loop [n num res []]
    (if (zero? n)
      res
      (recur (quot n 10) (cons (mod n 10) res)))))

(defn resolve-parameter
  [mode-mode codelist position parameter-idx mode relative-base]
  (let [parameter (nth codelist (+ parameter-idx position) 0)]
    (case mode-mode
      :interpreted (case mode
                     0 (nth codelist parameter 0) ; position mode
                     1 parameter ; immediate mode
                     2 (nth codelist (+ relative-base parameter) 0); relative mode
                     )
      :literal (case mode
                 0 parameter
                 1 parameter
                 2 (+ relative-base parameter)))))

(defn parse-instruction [instr]
  (let [digits (num-to-digits instr)
        complete-instr (concat (repeat (- 5 (count digits)) 0) digits)
        ;_ (prn (take-last 2 complete-instr))
        ;_ (prn (apply str (take-last 2 complete-instr)))
        ;_ (prn (read-string (apply str (take-last 2 complete-instr))))
        ;_ (flush)
        parsed (concat (take 3 complete-instr) [(Integer/parseInt (apply str (take-last 2 complete-instr)))])]
    parsed))

(defn save-to-code [codelist dest value]
  (let [codelist (if (> dest (count codelist))
                   (vec (concat codelist (take (- dest (count codelist)) (cycle [0]))))
                   codelist)]
    (assoc codelist dest value)))

;(def stop-var (atom false))


(defn uuid [] (.toString (java.util.UUID/randomUUID)))

(defn process-intcode
  ([c i o] (process-intcode c i o (uuid)))
  ([c i] (process-intcode c i nil nil))
  ([codelist-og input-channel output-channel id]
   (loop [codelist (vec codelist-og)
          position 0
          relative-base 0
          output '()]
     (let [[mode3 mode2 mode1 opcode] (parse-instruction (nth codelist position))]
       (cond
         (= 99 opcode) (do
                         (close! input-channel)
                         (when (not (nil? output-channel))
                           (close! output-channel))
                         output)
         :else (condp some [opcode]
                 #{1 2 7 8} (let [arg1 (resolve-parameter :interpreted codelist position 1 mode1 relative-base)
                                  arg2 (resolve-parameter :interpreted codelist position 2 mode2 relative-base)
                                  dest (resolve-parameter :literal codelist position 3 mode3 relative-base)]
                                        ;(println "Operate over" arg1 "and" arg2 "save to" dest)
                              (recur
                               (save-to-code codelist dest (case opcode
                                                             1 (+ arg1 arg2)
                                                             2 (* arg1 arg2)
                                                             7 (if (< arg1 arg2) 1 0)
                                                             8 (if (= arg1 arg2) 1 0)))
                               (+ position 4)
                               relative-base
                               output))
                 #{3} (let [dest (resolve-parameter :literal codelist position 1 mode1 relative-base)]
                        (recur
                         (save-to-code codelist dest (<!! input-channel))
                         (+ position 2)
                         relative-base
                         output))
                 #{4} (let [output-value (resolve-parameter :interpreted codelist position 1 mode1 relative-base)]
                          ;;(printf "[%s] Put %s on channel\n" id output-value)
                        (recur
                         codelist
                         (+ position 2)
                         relative-base
                         (do
                           (when (not (nil? output-channel))
                             (>!! output-channel output-value))
                           (conj output output-value))))
                 #{5 6} (let [value (resolve-parameter :interpreted codelist position 1 mode1 relative-base)
                              jump-dest (resolve-parameter :interpreted codelist position 2 mode2 relative-base)]
                          (recur
                           codelist
                           (if (or (and (= opcode 5) (not (zero? value)))
                                   (and (= opcode 6) (zero? value)))
                             jump-dest
                             (+ position 3))
                           relative-base
                           output))
                 #{9} (let [value (resolve-parameter :interpreted codelist position 1 mode1 relative-base)]
                        (recur
                         codelist
                         (+ position 2)
                         (+ relative-base value)
                         output))))))))

(defn ex1
  ([]
   (ex1 (read-input) 1)) ;; => 16225258
  ([data input]
   (let [in-ch (chan 10)]
     (>!! in-ch input)
     (first (process-intcode data in-ch)))))

(defn ex2
  ([]
   (ex2 (read-input) 5)) ;; => 2808771
  ([data input]
   (let [in-ch (chan 10)]
     (>!! in-ch input)
     (first (process-intcode data in-ch)))))

(defn -main []
  {"Day" "05" "Description" "" "Part1" (ex1) "Part2" (ex2)})
