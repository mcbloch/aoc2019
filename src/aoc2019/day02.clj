(ns aoc2019.day02
  (:require [clojure.java.io :as io]
            [clojure.math.combinatorics :as combo])
  (:gen-class))

(defn read-input []
  "Read file, split into array, cast every line to integers"
  (map read-string (-> (slurp
                        (-> "input_2.txt" io/resource io/file))
                       (clojure.string/split #","))))

(defn process-intcode [codelist-og]
  (loop [codelist codelist-og
         position 0]
    (let [opcode (nth codelist position)]
      (if (= 99 opcode)
        codelist
        (let [arg1 (nth codelist (nth codelist (+ 1 position)))
              arg2 (nth codelist (nth codelist (+ 2 position)))
              dest (nth codelist (+ 3 position))]
          (recur
           (case opcode
             1 (assoc codelist dest (+ arg1 arg2))
             2 (assoc codelist dest (* arg1 arg2)))
           (+ position 4)))))))

(defn ex1
  ([]
   (ex1 (read-input))) ;; => 6087827
  ([input]
   (-> input
       vec
       (assoc 1 12)
       (assoc 2 2)
       (process-intcode)
       first)))

(defn ex2
  ([]
   (ex2 (read-input))) ;; => 5379
  ([input]
   (let [codelist (vec input)
         vars (combo/cartesian-product (range 0 99) (range 0 99))]
     (loop [[noun verb] (first vars)
            rest-vars (next vars)]
       ;; (prn noun verb)
       (if (or (nil? noun) (nil? verb))
         (prn "Something went wrong")
         (let [result-list (-> codelist
                               (assoc 1 noun)
                               (assoc 2 verb)
                               process-intcode)]
           (if (= 19690720 (first result-list))
             (+ verb (* 100 noun))
             (recur (first rest-vars) (next rest-vars)))))))))

(defn -main [] {"Day" "02" "Description" "Intcode program result" "Part1" (ex1) "Part2" (ex2)})
