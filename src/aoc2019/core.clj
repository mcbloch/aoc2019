(ns aoc2019.core
  (:require [clojure.pprint :refer [print-table]]
            [aoc2019.day01]
            [aoc2019.day02]
            [aoc2019.day03]
            [aoc2019.day04]
            [aoc2019.day05]
            [aoc2019.day06]
            ;[aoc2019.day07]
            [aoc2019.day08])
  (:gen-class))

(defn -main
  [& args]
  (print-table ["Day" "Description" "Part1" "Part2"]
               [(aoc2019.day01/-main)
                (aoc2019.day02/-main)
                (aoc2019.day03/-main)
                (aoc2019.day04/-main)
                (aoc2019.day05/-main)
                (aoc2019.day06/-main)
                (aoc2019.day08/-main)]))
