(ns aoc2019.day11
  (:require
   [clojure.java.io :as io]
   [aoc2019.day05 :refer [process-intcode read-opcode-from-file]]
   [quil.core :as q]
   [clojure.core.async :as a]))

(def lookat-move-offset {:up    [0  -1]
                         :right [1  0]
                         :down  [0  1]
                         :left  [-1  0]})
(def direction-lookat-transition
  {0 {:up :left
      :right :up
      :down :right
      :left :down}
   1 {:up :right
      :right :down
      :down :left
      :left :up}})

(defn paintbot
  [data startcolor]
  (let [in-ch (a/chan 1000)
        out-ch (a/chan 1000)]
    (a/>!! in-ch startcolor)
    (a/go (process-intcode data in-ch out-ch))
    (loop [position [0 0]
           lookat :up
           pannels {}]
      (let [color (a/<!! out-ch)
            direction (a/<!! out-ch)]
        (if (nil? color)
          pannels
          (do
            (let [new-lookat (get-in direction-lookat-transition [direction lookat])
                  new-position (map + position (new-lookat lookat-move-offset))]
              (a/>!! in-ch (get pannels new-position 0))
              (recur
               new-position
               new-lookat
               (assoc pannels position color)))))))))

(defn ex1
  ([]
   (ex1 (read-opcode-from-file "input_11.txt"))) ;; => 1894
  ([data]
   (count (paintbot data 0))))

(defn draw [x y]
  (q/stroke 10)             ;; Set the stroke colour to a random grey
  (q/stroke-weight 5)       ;; Set the stroke thickness randomly
  (q/fill 10)               ;; Set the fill colour to a random grey

  (let [diam 10]             ;; Set the diameter to a value between 0 and 100
    (q/ellipse (+ (* x 10) 100) (+ (* y 10) 100) diam diam)))         ;; Draw a circle at x y with the correct diameter

(defn setup [pixels]
  (q/frame-rate 1)                    ;; Set framerate to 1 FPS
  (q/background 200)
  (q/text "Registration identifier" 100 100 3)
  (doseq [[x y] pixels]
    (draw x y)))                 ;; Set the background colour to

(defn do-drawing [pixels]
  (q/defsketch example                  ;; Define a new sketch named example
    :title "Registration identifier"    ;; Set the title of the sketch
    :settings #(q/smooth 2)             ;; Turn on anti-aliasing
    :setup #(setup pixels)                        ;; Specify the setup fn
    ;:draw draw                          ;; Specify the draw fn
    :size [700 400])                    ;; You struggle to beat the golden ratio
)

(defn ex2
  ([]
   (ex2 (read-opcode-from-file "input_11.txt")))
  ([data]
   (->> (paintbot data 1)
        (filter (fn [[k v]] (= 1 v)))
        (map (fn [[k v]] k))
        do-drawing)))

(defn -main [] {"Day" "11" "Description" "" "Part1" (ex1) "Part2" (ex2)})
