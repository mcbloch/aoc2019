(ns aoc2019.day09
  (:require
   [clojure.java.io :as io]
   [aoc2019.day05 :refer [process-intcode read-opcode-from-file]]
   [clojure.core.async :refer [chan >!!]]))

(defn ex1
  ([]
   (ex1 (read-opcode-from-file "input_9.txt"))) ;; => (2399197539)
  ([data]
   (let [ch (chan 10)]
     (>!! ch 1)
     (process-intcode data ch))))

(defn ex2
  ([]
   (ex2 (read-opcode-from-file "input_9.txt"))) ;; => (35106)
  ([data]
   (let [ch (chan 10)]
     (>!! ch 2)
     (process-intcode data ch))))


(defn -main [] {"Day" "09" "Description" "" "Part1" (ex1) "Part2" (ex2)})
