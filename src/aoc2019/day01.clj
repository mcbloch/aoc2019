(ns aoc2019.day01
  (:require [clojure.java.io :as io]))

(defn read-input []
  "Read file, split into array, cast every line to integers"
  (map read-string (-> (slurp
                        (-> "input_1.txt" io/resource io/file))
                       (clojure.string/split-lines))))

(defn calculate-fuel [mass]
  (-> mass
      (#(/ % 3))
      Math/floor
      (#(- % 2))
      Math/round))

(defn calculate-fuel-inclusive
  [mass]
  (let [new-fuel (calculate-fuel mass)]
    (if (<= new-fuel 0) 0 (+ new-fuel (calculate-fuel-inclusive new-fuel)))))

(defn ex1 []
  (->> (read-input)
       (map calculate-fuel)
       (reduce + 0))) ;; => 3315133


(defn ex2
  "Takes an amount of start fuel and calculates the final total of needed fuel counting for the added weight of adding fuel"
  []
  (->> (read-input)
       (map calculate-fuel-inclusive)
       (reduce + 0)));; => 4969831

(defn -main []
  (let [res1 (str (ex1))
        res2 (str "(inclusive) " (ex2))]
    {"Day" "01"
     "Description" "Needed fuel"
     "Part1" res1
     "Part2" res2})
  )
