(ns aoc2019.day10
  (:require [clojure.java.io :as io]
            [clojure.walk :refer [walk]]
            [clojure.math.numeric-tower :refer [abs]]
            [quil.core :as q]
            [quil.middleware :as m]
            [clojure.core.cache.wrapped :as c]))

(defn read-input [filename]
  (-> filename
      io/resource
      io/file
      slurp
      clojure.string/split-lines
      ((fn [row] (map #(clojure.string/split % #"") row)))))

(defn check-collisions [[x-from y-from] data]
  (for [[y row] (map-indexed vector data)]
    (for [[x cell] (map-indexed vector row)
          :when (and (= "#" cell)
                     (or (not= x x-from) (not= y y-from)))]
      (let [diff-x (- x x-from)
            diff-y (- y y-from)]
        (java.lang.Math/atan2 diff-y diff-x)))))

(defn ex1
  ([] (ex1 (read-input  "input_10.txt")))
  ([data]
   (->> (for [[y row] (map-indexed vector data)]
          (for [[x cell] (map-indexed vector row)
                :when (= "#" cell)]
            {:coordinate [x y]
             :visible-meteors (count (distinct (flatten (check-collisions [x y] data))))}))
        flatten
        (apply max-key :visible-meteors)))) ;; => {:coordinates [20 18], :visible-meteors 280}

(defn time-ex1 []
  (time (ex1))) ;; => +- 350msecs

(defn vaporize-meteorites
  ([filename] (let [data (read-input filename)]
                (vaporize-meteorites data (:coordinate (ex1 data)))))
  ([data [x-from y-from]]
   (let [start-angle (java.lang.Math/atan2 -1 0)]
     (->> (for [[y row] (map-indexed vector data)]
            (for [[x cell] (map-indexed vector row)
                  :when (and (= "#" cell)
                             (or (not= x x-from) (not= y y-from)))]
              (let [diff-x (- x x-from)
                    diff-y (- y y-from)]
                {:coordinate [x y]
                 :distance (+ (abs diff-x) (abs diff-y))
                 :angle (let [angle (java.lang.Math/atan2 diff-y diff-x)]
                          (if (< angle start-angle)
                            (+ angle (* 2 (java.lang.Math/PI)))
                            angle))})))
          flatten
          (group-by :angle)
          (into (sorted-map))
          seq
          (map (fn [[angle v]] (vec (sort-by :distance v))))
          vec
          ((fn [meteors-og]
             (loop [destroyed-count 1
                    vaporized-meteorites []
                    index 0
                    meteors meteors-og]
               (if (empty? meteors)
                 vaporized-meteorites
                 (let [vaporized-meteor (first (nth meteors index))
                       new-meteors
                       (let [this-angle (rest (nth meteors index))]
                         (if (empty? this-angle)
                           (vec (concat (subvec meteors 0 index)
                                        (when (< (inc index) (count meteors))
                                          (subvec meteors (inc index)))))
                           (assoc meteors index this-angle)))]
                   ;(printf "Vaporizing %dth asteroid at %s\n" destroyed-count vaporized-meteor)
                   (recur
                    (inc destroyed-count)
                    (conj vaporized-meteorites vaporized-meteor)
                    (let [next-i (if (= (count new-meteors) (count meteors)) (inc index) index)]
                      (if (>= next-i (count new-meteors))
                        0
                        next-i))
                    new-meteors))))))))))

(defn update-drawing [{:keys [all-meteorites remaining-meteorites] :as state}]
  (update state :remaining-meteorites rest))

(defn scale [pos]
  (-> pos
      (* 10)
      (+ 100)))

(defn draw-meteor [{[x y] :coordinate} color]
  (q/stroke color color)
  ;(q/stroke-weight 0)
  (q/fill color)
  (let [diam 10]             ;; Set the diameter to a value between 0 and 100
    (q/ellipse (scale x) (scale y) diam diam)))

(defn draw-laser [[x y] angle]
  (let [color (q/color 255 0 100)]
    (q/stroke color)
    (q/stroke-weight 2)
    (q/fill color)
    (let [diam 10]             ;; Set the diameter to a value between 0 and 100
      (q/line (scale x) (scale y) (scale (+ x (* 40 (Math/cos angle)))) (scale (+ y (* 40 (Math/sin angle))))))))

(defn draw [{:keys [all-meteorites remaining-meteorites center] :as state}]
  (q/clear)
  (q/background 200)
  (doseq [meteor all-meteorites]
    (draw-meteor meteor (q/color 180)))
  (draw-meteor {:coordinate center} (q/color 255 0 0))
  (when (not (empty? remaining-meteorites))
   (draw-laser center (:angle (first remaining-meteorites))))
  (doseq [meteor remaining-meteorites]
    (draw-meteor meteor (q/color 40))))

(defn setup [meteorites center]
  (q/frame-rate 10)
  (q/background 200)
  {:all-meteorites meteorites
   :remaining-meteorites meteorites
   :center center})

(defn do-drawing [meteorites center]
  (q/defsketch example                  ;; Define a new sketch named example
    :title ""    ;; Set the title of the sketch
    :settings #(q/smooth 2)             ;; Turn on anti-aliasing
    :setup #(setup meteorites center)                        ;; Specify the setup fn
    :draw draw                          ;; Specify the draw fn
    :update update-drawing
    :size [440 440]
    :middleware [m/fun-mode]))

(defn ex2
  ([] (ex2 "input_10_test.txt"))
  ([filename] (let [data (read-input filename)]
                (ex2 data (:coordinate (ex1 data)))))
  ([data coord]
   (let [meteorites (vaporize-meteorites data coord)]
     (println (nth meteorites 1))
     (do-drawing meteorites coord)
     (-> meteorites
         (nth 199)
         :coordinate
         ((fn [[x y]] (+ (* 100 x) y)))))))

(defn -main []
  {"Day" "10"
   "Description" ""
   "Part1" (ex1)
   "Part2" (ex2)})
