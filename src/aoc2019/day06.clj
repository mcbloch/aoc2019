(ns aoc2019.day06
  (:require [clojure.java.io :as io]))

(defn long-str [& strings] (clojure.string/join "\n" strings))

(defn read-input
  "Read file, split into array, cast every line to integers"
  ([]
   (-> "input_6.txt"
       io/resource
       io/file
       slurp
       read-input))
  ([data]
   (-> data
       (clojure.string/split-lines)
       ((fn [lines] (map (fn [line] (clojure.string/split line #"\)")) lines))))))

(defn gather-children
  [node relations]
  (when (not (nil? node))
    (let [children (->> relations
                        (filter (fn [[p c]] (= p node)))
                        (map (fn [[p c]] c)))
          recursed (for [child children] (gather-children child relations))]
      (merge {node children} (apply merge recursed)))))

(defn count-orbits
  "Takes a dictionary of nodes mapped on their children and calculates the amount of direct and indirect orbits
  This is a nice linear method over the amount of nodes
  It will recursively go down the tree and will the sum up the amount of orbits for every node from the bottom up
  "
  [node grouped]
  (let [children (get grouped node)
        deeper-nodes (for [child children] (count-orbits child grouped))]
    ;(printf " %s: #children below is %d\n" node (apply + (map (fn [l] (count l)) deeper-nodes)))
    ;(printf "    deeper nodes are %s\n" (seq (flatten deeper-nodes)))
    ;(printf "    returning %s\n" (cons (apply + (map (fn [l] (count l)) deeper-nodes))
                                  ;(flatten deeper-nodes)))
    (cons (reduce + (map (fn [l] (count l)) deeper-nodes))
          (flatten deeper-nodes))))

(defn ex1
  "Sum of amount of nodes under each node"
  ([] (ex1 (read-input)))
  ([input]
   ;(println "= EX1 =")
   (let [grouped-children (gather-children "COM" input)]
     (reduce + (count-orbits "COM" grouped-children)))))

(defn ex1-timed []
  (time (ex1)))
;; => 144909
;; => "Elapsed time: 350.301709 msecs"

(defn build-path [input tonode]
  (take-while identity (rest (iterate input tonode))))

(defn transfer-count [from to node-data]
  (let [s-from-path (set (build-path node-data from))
        s-to-path (set (build-path node-data to))]
    (count (clojure.set/union
            (clojure.set/difference s-from-path s-to-path)
            (clojure.set/difference s-to-path s-from-path)))))

(defn faster? []
  (let [input (read-input)
        node-data (into {} (map (comp vec reverse) input))]
    (reduce + (map count (for [k (keys node-data)] (build-path node-data k))))))

(defn faster?-timed []
  (time (faster?)))
;; => 144909
;; => "Elapsed time: 76.078228 msecs"

(defn ex2
  ([] (ex2 (read-input)))
  ([input]
   (let [node-data (into {} (map (comp vec reverse) input))]
     (transfer-count "YOU" "SAN" node-data))))

(defn ex2-timed []
  (time (ex2)))
;; => 259
;; => "Elapsed time: 15.055782 msecs"

(defn -main []
  {"Day" "06"
   "Description" "Universal Orbit Map"
   "Part1" (ex1)
   "Part2" (ex2)})
