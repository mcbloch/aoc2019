(ns aoc2019.user
  (:require [clojure.test :refer [run-tests]]
            [aoc2019.core :refer [-main]]))

(defn refresh-test [namespace]
  (require 'namespace :reload-all)
  (run-tests 'namespace))