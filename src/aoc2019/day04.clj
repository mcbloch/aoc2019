(ns aoc2019.day04
  (:require [clojure.java.io :as io]
            [clojure.math.combinatorics :as combo]
            [clojure.math.numeric-tower :as math]
            [clojure.core.reducers :as r]))

(defn read-input []
  (-> "input_4.txt"
      io/resource
      io/file
      slurp
      clojure.string/trim-newline
      (clojure.string/split #"-")
      ((fn [nums] (map read-string nums)))))

(defn num-to-digits
  [num]
  (loop [n num res []]
    (if (zero? n)
      res
      (recur (quot n 10) (cons (mod n 10) res)))))

(defn is-not-decreasing [number-list]
  (not (nil? (reduce (fn [acc num] (if (and (not (nil? acc)) (>= num acc)) num nil)) number-list))))

(defn at-least-2-adjecent [num-list]
  (true? (reduce (fn [acc num] (if (or (true? acc) (= acc num))
                                 true
                                 num)) num-list)))

(defn exact-2-adjecent [num-list]
  (true? (reduce (fn [acc num]
                   (cond
                     (true? acc) true
                     (= num (:last acc)) (update acc :count inc)
                     (= 2 (:count acc)) true
                     :else {:last num :count 1}))
                 {:last (first num-list)
                  :count 1}
                 (next (concat num-list [nil])))))

(defn check-password [pass]
  ((every-pred
    is-not-decreasing
    at-least-2-adjecent) (num-to-digits pass)))

(defn check-password-ex2 [pass]
  ((every-pred
    is-not-decreasing
    exact-2-adjecent) (num-to-digits pass)))

(defn ex1
  []
  (->> (read-input)
       (apply range)
       (filter check-password)
       count)) ;; => 1855

(defn ex2
  ([]
   (ex2 (read-input)))
  ([[lower upper :as bounds]]
   (if (> lower upper) (println "No")
       (->> bounds
            (apply range)
            (r/filter check-password-ex2)
            (r/map (fn [&arg] 1))
            (r/fold +))))) ;; => 1253

(defn -main [] {"Day" "04"
                "Description" "All the possible password combinations"
                "Part1" (ex1)
                "Part2" (ex2)})
